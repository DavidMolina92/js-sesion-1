1. Multiplica 10 por 5 y muestra el resultado mediante console.log.

2. Divide 10 por 2 y muestra el resultado en un console.log.

3. Muestra mediante un console.log el resto de dividir 15 por 9

4. Usa el correcto operador de asignación que resultará en ``x = 15``, teniendo dos variables ``y = 10`` y ``z = 5``.

5. Usa el correcto operador de asignación que resultará en ``x = 50``, teniendo dos variables ``y = 10`` y ``z = 5``.