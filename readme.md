1. Clonar este repositorio
2. Una vez clonado, desde la consola tienes que sacar una rama con tu nombre y apellido. Todo el código que hagas, será dentro de tu rama. Usaremos el siguiente comando: `git checkout -b nombre-apellido`
3. Programa tu código.
4. Tienes que ir haciendo commits mientras programas.
5. Una vez terminado, sube el código al repositorio con el comando `git push`.
6. Al ser una nueva rama, te pedirá que introduzcas el siguiente comando para subirla `git push --set-upstream origin tunombre-tuapellido` (puedes copiar este comando desde la consola si has ejecutado `git push`)