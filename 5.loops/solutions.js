// 1.
// Your code here...

for (var i = 0; i < 10; i++) {
    console.log(i);
}

// 2.
// Your code here...

for (var i = 0; i < 10; i++) {
    if (i % 2 == 0){
        console.log(i);
    }
}

// 3.
// Your code here...

for (var i = 0; i <= 10; i++) {
    if (i === 10) {
        console.log("Dormido!")
    }
    else {
        console.log("Intentando dormir");
    }
}