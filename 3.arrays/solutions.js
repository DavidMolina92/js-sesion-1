// 1.
// Your code here...

const cars = ["Saab", "Volvo", "BMW"];

console.log(cars[1]);

// 2.
// Your code here...

cars[0] = "Ford"; 

// 3.
// Your code here...

console.log(cars.length);

// 4.
// Your code here...

const rickAndMortyCharacters = ["Rick", "Beth", "Jerry"];

rickAndMortyCharacters.push("Morty");
rickAndMortyCharacters.push("Summer");

// 5.
// Your code here...

rickAndMortyCharacters.pop();
console.log(rickAndMortyCharacters[0], rickAndMortyCharacters[rickAndMortyCharacters.length -1]);

// 6.
// Your code here...


rickAndMortyCharacters.splice(1,1)
console.log(rickAndMortyCharacters);