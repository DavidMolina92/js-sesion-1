var fruits = ["Strawberry", "Banana", "Orange", "Apple"];

// Esta es mi entrada de datos
var foodSchedule = [
    { name: "Salad", isVegan: true },
    { name: "Salmon", isVegan: false },
    { name: "Tofu", isVegan: true },
    { name: "Burger", isVegan: false },
    { name: "Rice", isVegan: true },
    { name: "Pasta", isVegan: true },
    { name: "Filetitos de pollo", isVegan: false },
];

var veganCounter = 0;

for (var evely = 0; evely < foodSchedule.length; evely++) {

    if (foodSchedule[evely].isVegan === false) {
        foodSchedule[evely].name = fruits[0];
        foodSchedule[evely].isVegan = true;
        veganCounter++;
    }
}

console.log(foodSchedule);




// Este debe ser mi resultado
// const foodSchedule = [
//     { name: "Salad", isVegan: true },
//     { name: "Strawberry", isVegan: true },
//     { name: "Tofu", isVegan: true },
//     { name: "Banana", isVegan: true },
//     { name: "Rice", isVegan: true },
//     { name: "Pasta", isVegan: true },
//     { name: "Orange", isVegan: true },
// ];
