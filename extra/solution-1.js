var menoresDeEdad = "Usuarios menores de edad: ";
var mayoresDeEdad = "Usuarios mayores de edad: ";


const users = [{name: "Abel", years: 43}, {name: "Maria", years: 18}, {name: "Pedro", years: 14}, {name: "Samantha", years: 32}, {name: "Raquel", years: 16}];

for (var i = 0; i < users.length; i++) {
    if (users[i].years < 18) {
        menoresDeEdad += users[i].name + ", ";
    }
    else {
        mayoresDeEdad += users[i].name + ", ";
    }
}

console.log (menoresDeEdad);
console.log (mayoresDeEdad);